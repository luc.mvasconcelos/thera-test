import { Input, Button, Row, Col } from "antd";
import { Container } from "./styles";
import logo from "../../Logo.svg";

const Login = () => (
  <Container>
    <Row justify="center" align="middle">
      <Col>
        {" "}
        <img src={logo} alt="logo" />
      </Col>
    </Row>
    <Row justify="center">
      <Col>
      <i class="las la-user-circle"></i>
        <Input size="large" placeholder="Usuário" />
      </Col>
    </Row>
    <Row justify="center">
      <Col>
      <i class="las la-key"></i>
        <Input size="large" placeholder="Senha" />
      </Col>
    </Row>
    <Row justify="center">
      <Col>
        {" "}
        <Button type="primary">LOGIN</Button>
      </Col>
    </Row>
  </Container>
);

export default Login;
