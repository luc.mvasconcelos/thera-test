import styled from 'styled-components'


export const Container = styled.div`
transform: scale(0.7);
background-color: #1E5084;
height: 100vh;
width: 100vw;
text-align: center;
:first-child > :first-child{
    padding-top: 160px;
    padding-bottom: 100px;
}
:first-child > :nth-child(2){
    padding-bottom: 36px;
}
:first-child > :nth-child(3){
    padding-bottom: 50px;
}

textarea:focus, input:focus{
    outline: none;
}

i {
    font-size: 63px;
    color: #FCCD2A;
    margin-left: -63px;
    transform: translateY(16px);
}

input{
    border-radius: 5px;
    border: none;
    padding-left: 24px;
    width: 380px;
    height: 63px;
    line-height: 63px;
    font-size: 24px;
    font-weight: bold;
    margin-left: 12px;
}

button{
    cursor: pointer;
    height: 63px;
    width: 189px;
    border-radius: 5px;
    border: none;
    font-size: 32px;
    font-color: red;
    color: #1E5084;
    background-color: #FCCD2A;
    font-weight: bold;
    :hover{
        background-color: #f0d26c;
    } 
    :active{
        background-color: #f7c102;
    }
}

`;