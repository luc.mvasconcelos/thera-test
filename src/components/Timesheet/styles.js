import styled, { css } from 'styled-components'


export const Container = styled.div`
i {
    font-size: 46px;
    color: #FCCD2A;
}
img{
    width: 117px;
}

span {
    font-weight: bold;
    color: #fff;
    font-size: 30px;
}
`;