import { Input, Button, Row, Col, Table, Space } from "antd";
import { Container } from "./styles";
import logo from "../../Logo.svg";

const Timesheet = () => (
  <Container>
    <Row >
      <Col span={4}>
        <img src={logo} alt="logo" />
      </Col>
      <Col span={5} style={{textAlign:"left"}}>
      <span>
          Olá, Lucas!
      </span>
      </Col>
 
      <Col span={15} style={{textAlign:"right"}}>
      <i class="las la-times-circle"></i>
      </Col>
    </Row>
  </Container>
);

export default Timesheet;
